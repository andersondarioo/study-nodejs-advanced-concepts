const pendingTimers = []
const pendingOSTasks = []
const pendingOperations = []

index.runContents()

const shouldContinue = () => {
    // Check one: Any pending setTimeout, setInterval or setImmediate?
    // Check two: Any pending OS tasks? (Like server listening to port)
    // Check three: Any pending long running operations? (Like fs module)

    return pendingTimers.length || pendingOSTasks.length || pendingOperations.length
}

while (shouldContinue()) {
    // 1) Look at pendintTimers (setTimeOut, setInterval)
    // 2) Look at pendingOSTasks (or callbacks)
    // 3) Pause execution and continue when...
    //      - a new pendingTasks is done
    //      - a new pengingOperations is done
    //      - a timer is about to complete
    // 4) Look at pedingTimers (setImmediate)
}
